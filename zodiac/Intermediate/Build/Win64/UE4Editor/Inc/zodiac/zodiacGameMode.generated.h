// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZODIAC_zodiacGameMode_generated_h
#error "zodiacGameMode.generated.h already included, missing '#pragma once' in zodiacGameMode.h"
#endif
#define ZODIAC_zodiacGameMode_generated_h

#define zodiac_Source_zodiac_zodiacGameMode_h_18_RPC_WRAPPERS
#define zodiac_Source_zodiac_zodiacGameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define zodiac_Source_zodiac_zodiacGameMode_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAzodiacGameMode(); \
	friend ZODIAC_API class UClass* Z_Construct_UClass_AzodiacGameMode(); \
public: \
	DECLARE_CLASS(AzodiacGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/zodiac"), ZODIAC_API) \
	DECLARE_SERIALIZER(AzodiacGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define zodiac_Source_zodiac_zodiacGameMode_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAzodiacGameMode(); \
	friend ZODIAC_API class UClass* Z_Construct_UClass_AzodiacGameMode(); \
public: \
	DECLARE_CLASS(AzodiacGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/zodiac"), ZODIAC_API) \
	DECLARE_SERIALIZER(AzodiacGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define zodiac_Source_zodiac_zodiacGameMode_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ZODIAC_API AzodiacGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AzodiacGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ZODIAC_API, AzodiacGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AzodiacGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ZODIAC_API AzodiacGameMode(AzodiacGameMode&&); \
	ZODIAC_API AzodiacGameMode(const AzodiacGameMode&); \
public:


#define zodiac_Source_zodiac_zodiacGameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ZODIAC_API AzodiacGameMode(AzodiacGameMode&&); \
	ZODIAC_API AzodiacGameMode(const AzodiacGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ZODIAC_API, AzodiacGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AzodiacGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AzodiacGameMode)


#define zodiac_Source_zodiac_zodiacGameMode_h_18_PRIVATE_PROPERTY_OFFSET
#define zodiac_Source_zodiac_zodiacGameMode_h_15_PROLOG
#define zodiac_Source_zodiac_zodiacGameMode_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zodiac_Source_zodiac_zodiacGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	zodiac_Source_zodiac_zodiacGameMode_h_18_RPC_WRAPPERS \
	zodiac_Source_zodiac_zodiacGameMode_h_18_INCLASS \
	zodiac_Source_zodiac_zodiacGameMode_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define zodiac_Source_zodiac_zodiacGameMode_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zodiac_Source_zodiac_zodiacGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	zodiac_Source_zodiac_zodiacGameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	zodiac_Source_zodiac_zodiacGameMode_h_18_INCLASS_NO_PURE_DECLS \
	zodiac_Source_zodiac_zodiacGameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID zodiac_Source_zodiac_zodiacGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
