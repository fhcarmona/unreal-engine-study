// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ZODIAC_zodiacCharacter_generated_h
#error "zodiacCharacter.generated.h already included, missing '#pragma once' in zodiacCharacter.h"
#endif
#define ZODIAC_zodiacCharacter_generated_h

#define zodiac_Source_zodiac_zodiacCharacter_h_22_RPC_WRAPPERS
#define zodiac_Source_zodiac_zodiacCharacter_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define zodiac_Source_zodiac_zodiacCharacter_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAzodiacCharacter(); \
	friend ZODIAC_API class UClass* Z_Construct_UClass_AzodiacCharacter(); \
public: \
	DECLARE_CLASS(AzodiacCharacter, APaperCharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/zodiac"), NO_API) \
	DECLARE_SERIALIZER(AzodiacCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define zodiac_Source_zodiac_zodiacCharacter_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAzodiacCharacter(); \
	friend ZODIAC_API class UClass* Z_Construct_UClass_AzodiacCharacter(); \
public: \
	DECLARE_CLASS(AzodiacCharacter, APaperCharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/zodiac"), NO_API) \
	DECLARE_SERIALIZER(AzodiacCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define zodiac_Source_zodiac_zodiacCharacter_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AzodiacCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AzodiacCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AzodiacCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AzodiacCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AzodiacCharacter(AzodiacCharacter&&); \
	NO_API AzodiacCharacter(const AzodiacCharacter&); \
public:


#define zodiac_Source_zodiac_zodiacCharacter_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AzodiacCharacter(AzodiacCharacter&&); \
	NO_API AzodiacCharacter(const AzodiacCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AzodiacCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AzodiacCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AzodiacCharacter)


#define zodiac_Source_zodiac_zodiacCharacter_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SideViewCameraComponent() { return STRUCT_OFFSET(AzodiacCharacter, SideViewCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AzodiacCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__RunningAnimation() { return STRUCT_OFFSET(AzodiacCharacter, RunningAnimation); } \
	FORCEINLINE static uint32 __PPO__IdleAnimation() { return STRUCT_OFFSET(AzodiacCharacter, IdleAnimation); }


#define zodiac_Source_zodiac_zodiacCharacter_h_19_PROLOG
#define zodiac_Source_zodiac_zodiacCharacter_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zodiac_Source_zodiac_zodiacCharacter_h_22_PRIVATE_PROPERTY_OFFSET \
	zodiac_Source_zodiac_zodiacCharacter_h_22_RPC_WRAPPERS \
	zodiac_Source_zodiac_zodiacCharacter_h_22_INCLASS \
	zodiac_Source_zodiac_zodiacCharacter_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define zodiac_Source_zodiac_zodiacCharacter_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	zodiac_Source_zodiac_zodiacCharacter_h_22_PRIVATE_PROPERTY_OFFSET \
	zodiac_Source_zodiac_zodiacCharacter_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	zodiac_Source_zodiac_zodiacCharacter_h_22_INCLASS_NO_PURE_DECLS \
	zodiac_Source_zodiac_zodiacCharacter_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID zodiac_Source_zodiac_zodiacCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
