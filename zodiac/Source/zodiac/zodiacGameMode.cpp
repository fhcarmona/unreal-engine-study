// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "zodiacGameMode.h"
#include "zodiacCharacter.h"

AzodiacGameMode::AzodiacGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = AzodiacCharacter::StaticClass();	
}
